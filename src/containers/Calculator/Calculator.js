import React from 'react';
import {StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';

import { KEYS } from '../../constants';

export default class Calculator extends React.Component {
  state = {
    inputText: '',
    pendingOperation: null,
    firstOperand: ''
  };


  handlerInput = text => {
    this.setState({
      inputText: text
    });
  };

  handlerButtonInput = text => {
    if (['+', '-', '*', '/', '%'].indexOf(text) > -1) {
      this.setState({
        pendingOperation: text,
        firstOperand: this.state.inputText,
        inputText: ''
      });

    } else  if (text === '=') {
      this.calculate();

    } else if (text === 'C' || text === 'CE') {
      this.setState({
        inputText: '',
        pendingOperation: null,
        firstOperand: ''
      });

    } else if (text === '<') {
      let cloneState = this.state.inputText;
      const inputText = cloneState.substring(0, cloneState.length - 1);

      this.setState({inputText});

    } else {
      const stateText = this.state.inputText;
      const inputText = stateText + text;

      this.setState({inputText});
    }
  };

  calculate = () => {
    let result = null;
    switch (this.state.pendingOperation) {
      case '+':
        result =  parseFloat(this.state.firstOperand) + parseFloat(this.state.inputText);
        result = result.toString();
        this.setState({
          inputText: result,
          pendingOperation: null,
          firstOperand: ''
        });
        break;
      case '-':
        result =  parseFloat(this.state.firstOperand) - parseFloat(this.state.inputText);
        result = result.toString();
        this.setState({
          inputText: result,
          pendingOperation: null,
          firstOperand: ''
        });
        break;
      case '*':
        result =  parseFloat(this.state.firstOperand) * parseFloat(this.state.inputText);
        result = result.toString();
        this.setState({
          inputText: result,
          pendingOperation: null,
          firstOperand: ''
        });
        break;
      case '/':
        result =  parseFloat(this.state.firstOperand) / parseFloat(this.state.inputText);
        result = result.toString();
        this.setState({
          inputText: result,
          pendingOperation: null,
          firstOperand: ''
        });
        break;
      case '%':
        result =  parseFloat(this.state.firstOperand) % parseFloat(this.state.inputText);
        result = result.toString();
        this.setState({
          inputText: result,
          pendingOperation: null,
          firstOperand: ''
        });
        break;
      default:
        return;
    }

  };

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          onChangeText={() => this.handlerInput()}
          value={this.state.inputText}
          style={styles.text}
        />
        <View style={styles.panel}>
          <View style={styles.row}>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[0])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[0]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[1])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[1]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[2])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[2]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[3])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[3]}</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.row}>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[4])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[4]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[5])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[5]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[6])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[6]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[7])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[7]}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.row}>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[8])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[8]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[9])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[9]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[10])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[10]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[11])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[11]}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.row}>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[12])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[12]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[13])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[13]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[14])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[14]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[15])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[15]}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.row}>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[16])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[16]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[17])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[17]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[18])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[18]}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handlerButtonInput(KEYS[19])}
              style={styles.button}
            >
              <Text style={styles.btnText}>{KEYS[19]}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  text: {
    backgroundColor: 'rgb(41,41,41)',
    height: 150,
    width: '100%',
    color: 'rgb(255,255,255)',
    fontSize: 48,
    textAlign: 'right',
    paddingTop: 50,
    paddingRight: 10
  },
  panel: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(41,41,41)'
  },
  row: {
    flex: 1,
    flexDirection: 'row'
  },
  button: {
    flex: 1,
    margin: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(224, 224, 224)'
  },
  btnText: {
    fontSize: 36
  }
});