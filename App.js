import React from 'react';
import Calculator from "./src/containers/Calculator/Calculator";

export default class App extends React.Component {
  render() {
    return <Calculator />;
  }
}
